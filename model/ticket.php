<?php
class ticket extends table{

    protected $id;
    protected $code;
    protected $titel;
    protected $inhalt;
    protected $id_user;
    protected $id_abteilung;
    protected $unterlagen;
    protected $created;
    protected $status;
    protected $user_viewed;
    protected $admin_viewed;
    protected $dringlichkeit;
    

    public function   updateUserViewed($id,$val)
    {
       
        $statut=connexion::getConnexion()->exec("UPDATE ticket SET user_viewed='$val' WHERE id=".$id);
        return $statut;
    }
    public function  updateAdminViewed($id,$val)
    {
        $statut=connexion::getConnexion()->exec("UPDATE ticket SET admin_viewed='$val' WHERE id=".$id);
        return $statut;
    }

    public function  ticket_comment($id)
    {
        $result=connexion::getConnexion()->query("SELECT 
        tc.*,user.name AS user_name,user.type AS user_type, abteilung.name as abteilung_name 
        , unterlagen.unterlage_name ,unterlagen.original_name as original_name ,unterlagen.unterlage_size as unterlage_size 
        FROM ticket_comment tc 
        JOIN ticket ON tc.id_ticket = ticket.id
        LEFT JOIN unterlagen ON unterlagen.id = tc.unterlagen
        JOIN abteilung ON abteilung.id = ticket.id_abteilung
        JOIN user ON tc.id_user = user.id
        WHERE tc.id_ticket =$id
        order by tc.id desc 
        limit 1"  );
  
    return $result->fetch(PDO::FETCH_OBJ);
    }
    public function  selectUserTicket($limit = null)
    {
        $data_limit = $limit ? $limit : 'Limit 6' ;
        $result=connexion::getConnexion()->query("SELECT 
        tick.*,user.name AS user_name, abteilung.name as abteilung_name
        FROM ticket tick 
        JOIN user ON user.id = tick.id_user
        JOIN abteilung ON abteilung.id = tick.id_abteilung
        where id_user =".Auth::user()["id"]."
        order by tick.id desc $data_limit"  );
  
    return $result->fetchAll(PDO::FETCH_OBJ);
    }

    public function  updateStatus($id,$val)
    {
        $statut=connexion::getConnexion()->exec("UPDATE ticket SET status='$val' WHERE id=".$id);
        return $statut;
    }


}

?>
	
<?php
$page = 'dashboard';


$type = Auth::user()["type"] === "0" ? "user" : "admin";


if (isset($_GET['p'])) {
$page = $_GET['p'];
}



?>
<div class="sidebar">
  <div class="main-menu">
    <div class="scroll">
      <ul class="list-unstyled">
        <li class="<?= $page=='dashboard'?'active':''?>">
          <a href="javascript:void(0)"  data-url="<?php echo $type?>/dashboard/index.php" class="url">
            <i class="glyph-icon iconsmind-Home">
            </i>
            Dashbord
          </a>
        </li>
     
      
        <li class="<?= $page=='ticket'?'active':''?>">
          <a href="javascript:void(0)" data-url="<?php echo $type?>/ticket/index.php" class="url">
            <i class="glyph-icon simple-icon-support ">
            </i> Ticket
          </a>
        </li>
   
        <li  id="Konfiguration" class="<?= $page=='abteilung' || $page=='lager'  || $page=='artikel'    ?'active':''?>">
          <a href="#Konfiguration"  class="submenu">
            <i class="glyph-icon iconsmind-Gear">
            </i> Konfiguration
          </a>
        </li>
     
        
      </ul>
    </div>
  </div>
  <div class="sub-menu">
    <div class="scroll">
      
     
      <ul class="list-unstyled" data-link="Konfiguration">
       
      <li>
          <a  href="javascript:void(0)" data-url="<?php echo $type?>/abteilung/index.php" class="url sub"> <i class="glyph-icon simple-icon-settings">
          </i>Abteilung</a>
        </li>
      </ul>
    </div>
  </div>
</div>
  <?php
if (isset($_POST['ajax'])) {
include('../../../eve.php');
}



$abteilung = new abteilung();
$abteilungen = $abteilung->selectAll();


?>
<div class="container-fluid disable-text-selection">
   <div class="row">
      <div class="col-12">
         <div class="mb-2">
            <h1>Ticket </h1>
            <div class="float-sm-right text-zero">
               <button type="button" class="btn btn-success  url notlink" data-url="user/dashboard/index.php" > <i class="glyph-icon simple-icon-arrow-left"></i></button>
            </div>
         </div>
         <div class="separator mb-5"></div>
      </div>
   </div>
   <div class="row">
      <div class="col align-self-start">
         <div class="card mb-3">
            <div class="card-body">
               <h5 class="mb-3">Neue Ticket erstellen</h5>
               <form id="addform" method="post" name="form_user/dashboard" enctype="multipart/form-data">
                  <input type="hidden" name="act" value="insert">
                  <div class="form-row">

                  <div class="form-group col-md-4 offset-md-4">
                        <label for="code_tag">Dringlichkeit :</label>
                        <select class="form-control select2-single"     name="dringlichkeit" id="dringlichkeit"  >
                           <option value="Niedrig">Niedrig</option>
                           <option value="Mittel">Mittel</option>
                           <option value="Höhe">Höhe</option>
                        </select>
                     </div>

                  <div class="form-group col-md-4 offset-md-4">
                        <label for="id_lager">Abteilung :</label>
                        <select class="form-control select2-single"     name="id_abteilung" id="id_abteilung"  >
                         
                           <?php 
                              foreach ($abteilungen as $row) { 
                                 echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                              }?>
                        </select>
                     </div>
                     
                  <div class="form-group col-md-4 offset-md-4">
                        <label for="zeitpunkt">Titel :</label>
                        <input type="text" class="form-control" placeholder="Titel" id="titel" name="titel" value="" >
                     </div>
                   
                    
                     
                     <div class="form-group col-md-4 offset-md-4">
                        <label for="code_tag">Inhalt :</label>
                       
                        <textarea class="form-control "     name="inhalt" id="inhalt"></textarea>
                     </div>
                     

                     <div class="form-group col-md-4 offset-md-4">
                                <label>Anhang :</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image">
                                        <label class="custom-file-label" for="inputGroupFile04">Wählen Sie Anhang</label>
                                    </div>
                                </div>
                     </div>

                    </div>  
                  <div class="offset-md-4 text-zero">
                     <button type="submit" class="btn btn-primary btn-lg  mr-1 ">Speichern</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>


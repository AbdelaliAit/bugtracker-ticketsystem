<?php
if (isset($_POST['ajax'])) {
    include('../../../eve.php');
}
$id = explode('?id=',$_SERVER["REQUEST_URI"]);


$ticket = new ticket();



$detail_ticket = $ticket->selectQuery("SELECT 
 tick.*,user.name AS user_name, abteilung.name as abteilung_name,
 unterlagen.unterlage_name as unterlage_name,unterlagen.unterlage_size as unterlage_size,
 unterlagen.original_name as original_name
 FROM ticket tick 
 JOIN user ON user.id = tick.id_user
 LEFT JOIN unterlagen ON unterlagen.id = tick.unterlagen
 JOIN abteilung ON abteilung.id = tick.id_abteilung
 WHERE tick.id =".$id[1]."
 order by tick.created ASC LIMIT 1")[0];


 
$ticket_comment = new ticket_comment();


$ticket_comments = $ticket->selectQuery("SELECT 
tc.*,user.name AS user_name,user.type AS user_type, abteilung.name as abteilung_name 
, unterlagen.unterlage_name ,unterlagen.original_name as original_name ,unterlagen.unterlage_size as unterlage_size 
FROM ticket_comment tc 
JOIN ticket ON tc.id_ticket = ticket.id
LEFT JOIN unterlagen ON unterlagen.id = tc.unterlagen
JOIN abteilung ON abteilung.id = ticket.id_abteilung
JOIN user ON tc.id_user = user.id
WHERE tc.id_ticket =".$id[1]."
order by tc.created ASC ");


$ticket = new ticket();
$ticket->updateUserViewed($id[1],1);


?>

<link rel="stylesheet" href="<?php echo BASE_URL; ?>asset/css/ticket.css">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>asset/css/ticket_style.css">
<div class="container-fluid disable-text-selection">
   <div class="row">
      <div class="col-12">
         <div class="mb-2">
            <h1>Ticket details </h1>
            <div class="float-sm-right text-zero">
               <button type="button" class="btn btn-success  url notlink" data-url="user/dashboard/index.php" > <i class="glyph-icon simple-icon-arrow-left"></i></button>
            </div>
         </div>
         <div class="separator mb-5"></div>
      </div>
   </div>
   <div class="row vertical-gap md-gap">
      <div class="col-lg-7">
         <div class="dx-box dx-box-decorated">
            <div class="dx-blog-post dx-ticket dx-ticket-open">
               <div class="dx-blog-post-box pt-30 pb-30">
                  <h2 class="h4 mnt-5 mb-9 dx-ticket-title"><?php echo $detail_ticket->titel ?></h2>
                  <!-- START: Breadcrumbs -->
                  <ul class="dx-breadcrumbs text-left dx-breadcrumbs-dark mnb-6 fs-14">
                     <li><a href="#"><?php echo $detail_ticket->abteilung_name ?></a></li>
                     <?php if($detail_ticket->unterlage_name  ) { ?>
                     <a target="_blank" href="<?php echo BASE_URL.'upload/unterlagen/'.$detail_ticket->unterlage_name ?>" 
                         class="ticket-file-download badge badge-success"> 
                     <i class="glyph-icon simple-icon-eye"></i> Datei öffnen</a>
                     <?php }?>
                  </ul>
                  <?php 
                                            
                                            if($detail_ticket->dringlichkeit=="Niedrig")
                                            {
                                                $titel="Niedrig";
                                                $class="badge badge-info";
                                            
                                            }if($detail_ticket->dringlichkeit=="Mittel")
                                            {
                                                $titel="Mittel";
                                                $class="badge badge-warning";
                                            }
                                            if($detail_ticket->dringlichkeit=="Höhe")
                                            {
                                                $titel="Höhe";
                                                $class="badge badge-danger";
                                            }
                                            ?>
                                            
                              
                                        <!-- END: Breadcrumbs -->
                                        <span class="ticket-status <?php echo $class  ?>"> <?php echo  $titel  ?></span>
                                        <?php 
                                            
                                            if($detail_ticket->status=="open")
                                            {
                                                $titel="OFFEN";
                                                $class="badge badge-info";

                                            }if($detail_ticket->status=="closed")
                                            {
                                                $titel="GESCHLOSSEN";
                                                $class="badge badge-danger";
                                            }
                                            if($detail_ticket->status=="resolved")
                                            {
                                                $titel="BEHOBEN";
                                                $class="badge badge-success";
                                            }
                                        ?>
               </div>
               <div class="dx-separator"></div>
               <div style="background-color: #fafafa;">
                  <ul class="dx-blog-post-info dx-blog-post-info-style-2 mb-0 mt-0">
                     <li><span><span class="dx-blog-post-info-title">Ticket Id</span><?php echo $detail_ticket->code ?></span></li>
                     <li><span><span class="dx-blog-post-info-title">Status</span><?php echo $titel ?></span></li>
                     <li><span><span class="dx-blog-post-info-title">Datum</span><?php echo  $detail_ticket->created ?></span></li>
                     <li><span><span class="dx-blog-post-info-title">Abteilung</span><?php echo $detail_ticket->abteilung_name ?></span></li>
                  </ul>
               </div>
               <div class="col-md-12">
                  <div class="pt-30 pb-30">
                     <h4 class="h4 mnt-5 mb-9 dx-ticket-title">Inhalt der Ticket</h4>
                     <p><?php echo htmlspecialchars_decode ($detail_ticket->inhalt) ?> </p>
                  </div>
               </div>
            </div>
            <div class="dx-separator"></div>
            <?php 
               foreach($ticket_comments as $ticket_comment){ 
               
                   if($ticket_comment->user_type=="0")
                   {
                       $img_profile="user.png";
                   }
                   if($ticket_comment->user_type=="1")
                   {
                       $img_profile="admin.png";
                   }
               ?>
            <div class="dx-comment dx-ticket-comment">
               <div>
                  <div class="dx-comment-img">
                     <img src="<?php echo BASE_URL ?>/asset/img/<?php echo $img_profile ?>" alt="">
                  </div>
                  <div class="dx-comment-cont">
                     <a href="javascript:void(0)" class="dx-comment-name"><?php echo  $ticket_comment->user_name ?></a>
                     <div class="dx-comment-date"><?php echo  $ticket_comment->created ?></div>
                     <div class="dx-comment-text">
                        <p class="mb-0"><?php echo  htmlspecialchars_decode ($ticket_comment->nachricht) ?></p>
                     </div>
                  <?php if($ticket_comment->original_name!=""){ ?>
                     <a target="_blank" href="<?php echo BASE_URL.'upload/unterlagen/'.$ticket_comment->unterlage_name ?>" class="dx-comment-file dx-comment-file-jpg">
                     <span class="dx-comment-file-img"><img src="assets/images/icon-jpg.svg" alt="" width="36"></span>
                     <span class="dx-comment-file-name"><?php echo $ticket_comment->original_name ?></span>
                     <span class="dx-comment-file-size"><?php echo unterlagen::formatSizeUnits($ticket_comment->unterlage_size) ?></span>
                     <span class="dx-comment-file-icon"><span class="icon pe-7s-download"></span></span>
                     </a>
                     <?php } ?>
                  </div>
               </div>
            </div>
            <?php } ?>   
         </div>
      </div>
      <div class="col-md-5 mb-5">
         <div class="dx-box dx-box-decorated">
            <div class="dx-blog-post dx-ticket dx-ticket-open">
               <div class="dx-blog-post-box pt-30 pb-30">
                  <h2 class="h4 mnt-5 mb-9 dx-ticket-title">Antwort auf dieses Ticket</h2>
                  <form id="replyform" method="post" name="" enctype="multipart/form-data">
                     <input type="hidden" name="act" value="reply">
                     <input type="hidden" name="id_ticket" value="<?php echo $detail_ticket->id ?> ">
                     <div class="form-row">
                        <div class="separator mb-5"></div>
                        <div class="form-group col-md-12">
                           <label for="code_tag">Inhalt :</label>
                           <textarea class="form-control "     name="nachricht" id="nachricht"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                           <label>Anhang :</label>
                           <div class="input-group">
                              <div class="custom-file">
                                 <input type="file" class="custom-file-input" id="image" name="image">
                                 <label class="custom-file-label" for="inputGroupFile04">Wählen Sie Anhang</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="text-center text-zero">
                        <button type="submit" class="btn btn-primary btn-lg  mr-1 ">Speichern</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script src="<?php echo BASE_URL; ?>asset/js/vendor/ckeditor5-build-classic/ckeditor.js"></script>


<script type="text/javascript">


    $( document ).ready(function() {

      ClassicEditor.create( document.querySelector( '#nachricht' ) )
  
  


        $(".select2-single").select2({
            theme: "bootstrap",
            placeholder: "",
            maximumSelectionSize: 6,
            containerCssClass: ":all:"
        });


          $("input.datepicker").datepicker({
        orientation: "bottom left",
        format: 'yyyy-mm-dd',
         templates: {
            leftArrow: '<i class="simple-icon-arrow-left"></i>',
            rightArrow: '<i class="simple-icon-arrow-right"></i>'
        }
    });



         


});
</script>





<script type="text/javascript">


    $( document ).ready(function() {
   

   

        $(".select2-single").select2({
            theme: "bootstrap",
            placeholder: "",
            maximumSelectionSize: 6,
            containerCssClass: ":all:"
        });



    $("#replyform" ).on( "submit", function( event ) {
         event.preventDefault();
         var form = $( this );
               $.ajax({
               type: "POST",
               url: "<?php echo BASE_URL.'views/user/dashboard/' ;?>controle.php",
               data: new FormData(this),
               dataType: 'text',
               cache: false,
               contentType: false,
               processData: false,
               success: function (data) {
                 console.log(data)
               if (data.indexOf("success")>=0) {
               swal(
               'Hinzufügen',
               'Antwort wurde erfolgreich gesendet',
               'success'
               ).then((result) => {
               history.replaceState({},"",`<?php echo BASE_URL."user/dashboard/ticket-details.php?id=".$detail_ticket->id; ?>` );

               $.ajax({
               method:'POST',
               data: {ajax:true},
               url: `<?php echo BASE_URL."views/user/dashboard/ticket-details.php?id=".$detail_ticket->id; ?>`,
               context: document.body,
               success: function(data) {
               $("#main").html( data );
               }
               });
         });
         }
         else{
               form.append(` <div id="alert-danger" class="alert  alert-danger alert-dismissible fade show rounded mb-0" role="alert">
               <strong>${data}</strong>
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">×</span>
               </button>
                 </div>`);
                  }
          }
         });
});
                


});
</script>

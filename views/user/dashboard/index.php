<?php
if (isset($_POST['ajax'])) {
include('../../../evr.php');
}
$abteilung = new abteilung();
$abteilungen = $abteilung->selectAll();

$_SESSION['from'] = 0;
// $result =  connexion::getConnexion()->query( " select  count(id) as total from warenbewegungen ");
// $totalwarenbewegungen  = $result->fetch(PDO::FETCH_OBJ);

$ticket = new ticket();

$tickets = $ticket->selectUserTicket();

?>
<link rel="stylesheet" href="<?php echo BASE_URL; ?>asset/css/user-ticket.css">
<div class="container-fluid disable-text-selection">
    <div class="row">
        <div class="col-12">
            <div class="mb-2">
                <h1>Dashbord</h1>
               
     		 </div>
           <div class="separator mb-5"></div>
        </div>
    </div>
    <div class="row ">

	<div class="col-md-7">
	<div class="chat_container">
		<div class="job-box">
			<div class="job-box-filter">
				<div class="row">
					<div class="col-md-6 col-sm-6">
					<label>Show 
					<select name="datatable_length" class="form-control input-sm">
					<option value="10">10</option>
					<option value="25">25</option>
					<option value="50">50</option>
					<option value="100">100</option>
					</select>
					entries</label>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="filter-search-box text-right">
							<label>Search:<input type="search" class="form-control input-sm" placeholder=""></label>
						</div>
					</div>
				</div>
			</div>
			<div class="inbox-message">
				<ul id="shouts">
         
            <?php
               if (isset($_POST['ajax'])) {
                  include("../partials/ticket-view.php"); 
               }
               else
               {
                  include("user/../partials/ticket-view.php"); 
               }
               
               ?>
				</ul>
			</div>
			<div class="text-center mt-5 text-zero">
                     <button id="loadmore" onclick="loadmore()" class="btn btn-success btn-lg   ">Mehr laden</button>
                  </div>
		</div>
	</div>
</div>
		<div class="col-md-5 mb-5">
			<div class="chat_container">
				<div class="job-box">
				
               <form id="addform" method="post" name="form_user/dashboard" enctype="multipart/form-data">
                  <input type="hidden" name="act" value="insert">
                  <div class="form-row">
				  <div class="form-group col-md-10 offset-md-1">
				  <h5 class="mt-4">Neue Ticket erstellen</h5>
				  </div>
				  <div class="separator mb-5"></div>
                  <div class="form-group col-md-10 offset-md-1">
                        <label for="code_tag">Dringlichkeit :</label>
                        <select class="form-control select2-single"     name="dringlichkeit" id="dringlichkeit"  >
                           <option value="Niedrig">Niedrig</option>
                           <option value="Mittel">Mittel</option>
                           <option value="Höhe">Höhe</option>
                        </select>
                     </div>

                  <div class="form-group col-md-10 offset-md-1">
                        <label for="id_lager">Abteilung :</label>
                        <select class="form-control select2-single"     name="id_abteilung" id="id_abteilung"  >
                         
                           <?php 
                              foreach ($abteilungen as $row) { 
                                 echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                              }?>
                        </select>
                     </div>
                     
                  <div class="form-group col-md-10 offset-md-1">
                        <label for="zeitpunkt">Titel :</label>
                        <input type="text" class="form-control" placeholder="Titel" id="titel" name="titel" value="" >
                     </div>
                   
                    
                     
                     <div class="form-group col-md-10 offset-md-1">
                        <label for="code_tag">Inhalt :</label>
                       
                        <textarea class="form-control "     name="inhalt" id="inhalt"></textarea>
                     </div>
                     

                     <div class="form-group col-md-10 offset-md-1">
                                <label>Anhang :</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image">
                                        <label class="custom-file-label" for="inputGroupFile04">Wählen Sie Anhang</label>
                                    </div>
                                </div>
                     </div>

                    </div>  
                  <div class="text-center text-zero">
                     <button type="submit" class="btn btn-primary btn-lg  mr-1 ">Speichern</button>
                  </div>
               </form>
				</div>
			</div>
		</div>
    </div>

</div>
     
   
            <script type="text/javascript">
            
            $(document).ready(function () {


 				$('#myTable').DataTable( {
                    responsive: true,
                    
                } );

             
          

           
            });

            </script>



<script src="<?php echo BASE_URL; ?>asset/js/vendor/ckeditor5-build-classic/ckeditor.js"></script>

<script type="text/javascript">


    $( document ).ready(function() {
      ClassicEditor
            .create( document.querySelector( '#inhalt' ) )
  

      $('#code_tag').each(function(index, value){
        if (value.className != 'en') {
        $(this).attr('Lang','fa');
        }
});


        $(".select2-single").select2({
            theme: "bootstrap",
            placeholder: "",
            maximumSelectionSize: 6,
            containerCssClass: ":all:"
        });



    $("#addform" ).on( "submit", function( event ) {
         event.preventDefault();
         var form = $( this );
               $.ajax({
               type: "POST",
               url: "<?php echo BASE_URL.'views/user/dashboard/' ;?>controle.php",
               data: new FormData(this),
               dataType: 'text',
               cache: false,
               contentType: false,
               processData: false,
               success: function (data) {
                 
               if (data.indexOf("success")>=0) {
               swal(
               'Hinzufügen',
               'Neues Ticket hat hinzugefügt',
               'success'
               ).then((result) => {
               history.replaceState({},"",`<?php echo BASE_URL."user/dashboard/index.php"; ?>` );

               $.ajax({
               method:'POST',
               data: {ajax:true},
               url: `<?php echo BASE_URL."views/user/dashboard/index.php"; ?>`,
               context: document.body,
               success: function(data) {
               $("#main").html( data );
               }
               });
         });
         }
         else{
               form.append(` <div id="alert-danger" class="alert  alert-danger alert-dismissible fade show rounded mb-0" role="alert">
               <strong>${data}</strong>
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">×</span>
               </button>
                 </div>`);
                  }
          }
         });
});
                

});

function loadmore(){
$.ajax({
   type: "POST",
   url: "<?php echo BASE_URL.'views/user/dashboard/' ;?>controle.php",
   data: {"act":"loadmore"},
   dataType: "text",
   success: (data) =>{
    console.log(data);
    $('#shouts').append(data).fadeIn();
   }
    });

}

</script>
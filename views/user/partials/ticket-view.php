<?php
               
               $ticket = new ticket();

               foreach($tickets as $tic) {
                  

                  $ticket_comment = $ticket->ticket_comment($tic->id);
                  
                  
                  ?>
					<li <?php echo !$tic->user_viewed ?" class='ticket-unread'" :"" ?> >
						<a href="javascript:void(0)" class="url notlink" data-url="user/dashboard/ticket-details.php?id=<?php echo $tic->id;?>">
							<div class="message-avatar">
                     <?php 
                           if($ticket_comment )
                           {
                              if($ticket_comment->user_type=="0") $img_profile="user.png";
                                  else   $img_profile="admin.png";
                               }
                           else{
                              $img_profile="user.png";
                           }
                     ?>
							    <img src="<?php echo BASE_URL ?>/asset/img/<?php echo $img_profile ?>" alt="">
                 	</div>
							<div class="message-body">
								<div class="message-body-heading">
									<h5><?php echo $tic->titel?> 
                        <?php 
                          if($tic->dringlichkeit=="Niedrig")
                          {
                              $titel="Niedrig";
                              $class="badge badge-info";
                          
                          }if($tic->dringlichkeit=="Mittel")
                          {
                              $titel="Mittel";
                              $class="badge badge-warning";
                          }
                          if($tic->dringlichkeit=="Höhe")
                          {
                              $titel="Höhe";
                              $class="badge badge-danger";
                          }?>   
                           <span class="<?php echo $class  ?>"><?php echo $titel?></span>
                        
                        </h5>
									<span><?php echo $ticket_comment ? $ticket_comment->user_name." :" :"" ?>  <?php echo $tic->created?></span>
								</div>
								<p><?php echo $ticket_comment ? substr(strip_tags( htmlspecialchars_decode($ticket_comment->nachricht)),0,110) . "...": substr(strip_tags( htmlspecialchars_decode($tic->inhalt) ),0,110) . "...";  ?> </p>
							</div>
						</a>
					</li>
				<?php }?>
<?php
include('../../../evr.php');


if($_POST['act']=='pagination') { 
  
	$ticket = new ticket();


	$start = !empty($_POST['page']) ? $_POST['page'] : 0;
	$limit = $_POST['limit'] ;

	//set conditions for search
	$depot_cat_sql = '';
	$whereSQL = $orderSQL = '';
	$keywords = $_POST['keywords'];
	$status = $_POST['status'];
	$dringlichkeit = $_POST['dringlichkeit'];
	$id_abteilung = $_POST['id_abteilung'];
	$zeitpunkt_von = $_POST['zeitpunkt_von'];
	$zeitpunkt_bis = $_POST['zeitpunkt_bis'];
	

	$date_des = '';

		if ($zeitpunkt_von !='' && $zeitpunkt_bis =="") {
			$date_des= " and DATEDIFF(tick.created ,'$zeitpunkt_von')>=0 ";
		}

		if ($zeitpunkt_bis !='' && $zeitpunkt_von =="") {
			$date_des= " and DATEDIFF(tick.created ,'$zeitpunkt_bis')<=0 ";
		}

		if ($zeitpunkt_von !='' && $zeitpunkt_bis !='' ) {
			$date_des= " and tick.created between '$zeitpunkt_von' and '$zeitpunkt_bis' ";
		}

  	$depot_cat_sql .= $date_des;

	if($id_abteilung != '0') {
		$depot_cat_sql .= " and tick.id_abteilung =  '$id_abteilung' ";
	 }
 

	if($status != '0') {
	   $depot_cat_sql .= " and tick.status =  '$status' ";
	}

	if($dringlichkeit != '0') {
		$depot_cat_sql .= " and tick.dringlichkeit =  '$dringlichkeit' ";
	 }

	
 
	


	if(!empty($keywords)) {
	   $whereSQL = "WHERE 1 " . $depot_cat_sql . " AND (abteilung.name LIKE '%" . $keywords . "%' OR status LIKE '%" . $keywords . "%' OR dringlichkeit LIKE '%" . $keywords . "%' OR user.name LIKE '%" . $keywords . "%'  OR tick.titel LIKE '%" . $keywords . "%' OR tick.inhalt LIKE '%" . $keywords . "%')";
	} else {
	   $whereSQL = "WHERE 1 " . $depot_cat_sql . " ";
	}
	if(!empty($sortBy)) {
	   $orderSQL = " ORDER BY created " . $sortBy;
	} else {
	   $orderSQL = " ORDER BY created ";
	}
	
	$orderSQL = " ORDER BY id DESC ";
	//get number of rows

	$datalimit = "";
	if($limit !=0)	$datalimit = "LIMIT $start,$limit";


	
		$query = $ticket->selectQuery("SELECT 
		 tick.*,user.name AS user_name, abteilung.name as abteilung_name
		FROM ticket tick 
		JOIN user ON user.id = tick.id_user
		JOIN abteilung ON abteilung.id = tick.id_abteilung

		$whereSQL
		order by tick.id asc $datalimit");
	

	
	
	$queryNum = $ticket->selectQuery("SELECT 
	tick.*,user.name AS user_name, abteilung.name as abteilung_name
	FROM ticket tick 
	JOIN user ON user.id = tick.id_user
	JOIN abteilung ON abteilung.id = tick.id_abteilung
	$whereSQL
	order by  tick.id asc");



	$rowCount = count($queryNum);

	//initialize pagination class
	$pagConfig = array(
	   'currentPage' => $start,
	   'totalRows' => $rowCount,
	   'perPage' => $limit,
	   'link_func' => 'searchFilter'
	);

	$pagination = new Pagination($pagConfig);
	//get rows
	//$query = $warenbewegungen->selectQuery("SELECT * FROM warenbewegungen $whereSQL $orderSQL LIMIT $start,$limit");

	include("../partials/table_ticket.php");
}

elseif ($_POST['act']=='reply') {
	try {
	
		$_POST["id_user"] = Auth::user()['id'];
    

         $dossier = '../../../upload/unterlagen/';
        if($_FILES['image']['name']!="")
        {
        $fichier = md5(uniqid());
        $extension = strrchr($_FILES['image']['name'], '.');
        if(move_uploaded_file($_FILES['image']['tmp_name'], $dossier .$fichier.$extension))
        {
        $_POST["original_name"]   = $_FILES['image']['name']; 
        $_POST["unterlage_size"]   = filesize($dossier.$fichier.$extension);
        $_POST["unterlage_name"  ] = $fichier.$extension;
        $unterlagen = new unterlagen();
     
        $res =  $unterlagen->insert();
        }
        }
        $_POST["unterlagen"] =  $res ? $unterlagen->laset_insert() : null;
       
        $ticket_comment = new ticket_comment();
        $res =  $ticket_comment->insert();

		$ticket = new ticket();
		$ticket_viewed = $ticket->updateUserViewed($_POST['id_ticket'],0);

		
		die('success');
		} catch (Exception $e) {
				die($e);
	}
}


elseif($_POST['act']=='status_bearbeiten')
{
	try {
		$ticket = new ticket();
		$ticket_updated = $ticket->updateStatus($_POST['id_ticket'],$_POST['status']);
		die('success');
	} catch (Exception $e) {
		die($e);
	}

}



?>
<?php
if (isset($_POST['ajax'])) {
   include('../../../evr.php');
}

$_SESSION['LIMIT'] = 10 ;
$limit = $_SESSION['LIMIT'];

$page = 'ticket';

$ticket = new ticket();

 $query = $ticket->selectQuery("SELECT 
 tick.*,user.name AS user_name, abteilung.name as abteilung_name
 FROM ticket tick 
 JOIN user ON user.id = tick.id_user
 JOIN abteilung ON abteilung.id = tick.id_abteilung

 order by tick.created ASC LIMIT 10");

 $queryNum = $ticket->selectQuery("SELECT COUNT(*) as numrows FROM ticket ");


 $pagConfig = array(
        'totalRows' =>$queryNum[0]->numrows,
        'perPage' => $limit,
        'link_func' => 'searchFilter'
     );
         
 $pagination = new Pagination($pagConfig);

 $abteilung = new abteilung();
 $abteilungen = $abteilung->selectAll();

?>
<style>
.loadingex{
 display:inline-block;
    width:30px;
    margin-top:15px;
    height:30px;
    border:2px solid rgba(20,83,136,.2);
    border-radius:50%;
    border-top-color:#333333;
    animation:b 1s ease-in-out infinite;
    -webkit-animation:b 1s ease-in-out infinite;
    display:none
}
</style>


<div class="container-fluid disable-text-selection">
<div class="row">
   <div class="col-12">
      <div class="mb-2">
         <h1>List ticket </h1>
        
      </div>
      <div class="separator mb-5"></div>
   </div>
</div>



<div class="row">
<div class="col-xl-12 col-lg-12 mb-4">
   <div class="card h-100">
      <div class="card-body">
         <form id="addform" target="_blank" method="post" name="form_ticket" action="<?php echo BASE_URL.'views/ticket/excel.php'?>" enctype="multipart/form-data">
            <div class="form-row">
               <div class="form-group   col-md-2">
                  <label> &nbsp;&nbsp;Stichwort : </label>
                  <input type="text" id="keywords"class="form-control" placeholder="Suche mit Schlüsselwort" onkeyup="searchFilter()"/>
               </div>
               <div class="form-group col-md-2">
                  <label for="limit">limit :</label>
                  <select class="form-control select2-single"   name="limit" id="limit"  >
                     <option value="10">10</option>
                     <option value="25">25</option>
                     <option value="50">50</option>
                     <option value="100">100</option>
                     <option value="200">200</option>
                     <option value="500">500</option>
                     <option value="1000">1000</option>
                     <option value="10000">10000</option>
                  </select>
               </div>

               <div class="form-group col-md-2">
                  <label for="code_tag">Status :</label>
                  <select class="form-control select2-single"     name="status" id="status"  >
                     <option value="0">Gemeinsame Ticket</option>
                     <option value="open">öffnen</option>
                     <option value="closed">schließen</option>
                     <option value="resolved">behoben</option>
                  </select>
               </div>
               <div class="form-group col-md-2">
                  <label for="id_client">Abteilung :</label>
                  <select class="form-control select2-single" name="id_abteilung" id="id_abteilung"  >
                     <option value="0">alle</option>
                     <?php 
                        foreach ($abteilungen as $row) { 
                           echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                        }?>
                  </select>
               </div>

               <div class="form-group col-md-2">
                  <label for="dringlichkeit">Dringlichkeit :</label>
                  <select class="form-control select2-single"  name="dringlichkeit" id="dringlichkeit"  >
                           <option value="0">alle</option>
                           <option value="Niedrig">Niedrig</option>
                           <option value="Mittel">Mittel</option>
                           <option value="Höhe">Höhe</option>
                  </select>
               </div>
            
            </div>
            <div class="row">
               <div class="form-group col-md-3">
                  <label for="date_DES_1">Zeitraums Von :</label>
                  <input type="text" class="form-control datepicker filter" id="zeitpunkt_von" name="zeitpunkt_von"  >
               </div>
               <div class="form-group col-md-3">
                  <label for="date_DES_2">Zeitraums bis :</label>
                  <input type="text" class="form-control datepicker filter" id="zeitpunkt_bis" name="zeitpunkt_bis"  >
               </div>
               <div class="form-group col-md-2 text-zero">
                  <button onclick="searchFilter()" type="button" class="btn btn-success default btn-lg btn-block  mr-1 " style="margin-top: 25px;">Submit</button>
               </div>
               <div class="form-group col-md-2 text-zero">
                  <button id="expotexcel" type="button" class="btn btn-success default btn-lg btn-block  mr-1 " style="margin-top: 25px;">Export Excel</button>
               </div>
               <div class="form-group col-md-2 text-zero">
                  <button id="expotexceltout" type="button" class="btn btn-success default btn-lg btn-block  mr-1 " style="margin-top: 25px;">Export Alle</button>
               </div>
              
            </div>
            <div class="row">
             
               <div class="loadingex"></div>
            </div>
         </form>
         <div id="posts_content">
            
               
               <?php
               if (isset($_POST['ajax'])) {
                  include("../partials/table_ticket.php"); 
               }
               else
               {
                  include("admin/../partials/table_ticket.php"); 
               }
               
               ?>
          
         </div>
      </div>
   </div>
</div>
                      
<script type="text/javascript">
            
    function searchFilter(page_num) {

       
            page_num = page_num ? page_num : 0;
            var keywords = $('#keywords').val();
            var status = $('#status').val();
            var dringlichkeit = $('#dringlichkeit').val();
            var id_abteilung = $('#id_abteilung').val();
            var zeitpunkt_von = $('#zeitpunkt_von').val();
            var zeitpunkt_bis = $('#zeitpunkt_bis').val();
            var limit = $('#limit').val();
            
            
             $('#posts_content').html('<div class="loading"></div>');


            $.ajax({
                type: 'POST',
                  url: "<?php echo BASE_URL.'views/admin/ticket/' ;?>controle.php",
                data: {
                "act":"pagination",
                "keywords":keywords,
                "page":page_num,
                "limit":limit,
                "status" : status,
                "dringlichkeit" : dringlichkeit,
                "id_abteilung" : id_abteilung,
                "zeitpunkt_von" : zeitpunkt_von,
                "zeitpunkt_bis" : zeitpunkt_bis,
                 },
                beforeSend: function () {
                    $('.loading-overlay').show();
                },
                success: function (html) {
                    $('#posts_content').html(html);

         table =  $('#myTable').DataTable( {
                    responsive: true,
                   
                  
                    bPaginate: false, 
                    bFilter: false, 
                    bInfo: false,
                   
                      
                      
         });
         

                }
            });
        }



            $(document).ready(function () {


 

 $(".select2-single").select2({
            theme: "bootstrap",
            placeholder: "",
            maximumSelectionSize: 6,
            containerCssClass: ":all:"
        });
                $("input.datepicker").datepicker({
                     format: 'yyyy-mm-dd',
                     templates: {
                     leftArrow: '<i class="simple-icon-arrow-left"></i>',
                     rightArrow: '<i class="simple-icon-arrow-right"></i>'
                    }
                })
 $('#myTable').DataTable( {
                    responsive: true,
                   
                  
                    bPaginate: false, 
                    bFilter: false, 
                    bInfo: false,
                } );
            
            $('body').on( "click",".delete", function( event ) {
             event.preventDefault();


                    var btn = $(this);
                swal({
                 title: 'Sind Sie sicher ?',
                  text: "Sind Sie sicher, dass Sie diese ticket löschen möchten?!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#d33',
                  cancelButtonColor: '#3085d6',
                  confirmButtonText: 'Ja, löschen !'
                }).then((result) => {
                  if (result.value) {

                $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'views/ticket/' ;?>controle.php",
                data: {act:"delete",id: btn.data('id')},
                success: function (data) {
                   
                   swal(
                      'Deleted',
                      'Ihre Datei wurde gelöscht.',
                      'success'
                    ).then((result) => {

                        btn.parents("td").parents("tr").remove();
                    });
                   
                }
            });
                    
                  }
                });
           
            });


        $('body').on( "click",".archive", function( event ) {
             event.preventDefault();


                    var btn = $(this);
                swal({
                 title: 'Sind Sie sicher?',
                  text: "Sind Sie sicher, dass Sie diese Ticket archivieren möchten? ",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#145388',
                  cancelButtonColor: '#3085d6',
                  confirmButtonText: 'Ja, archivieren!'
                }).then((result) => {
                  if (result.value) {

                $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'views/admin/ticket/' ;?>controle.php",
                data: {act:"archive",id: btn.data('id'),val:btn.data('arc')},
                success: function (data) {
                   
                               swal(
                                 "Archiviert",
                                  'Ihre Warrenbewegung wurde archiviert.',
                                  'success'
                                ).then((result) => {
                                    btn.parents("td").parents("tr").remove();
                                });
                               
                            }
                        });
                    
                  }
                });
           
            });


              $('body').on( "click",".static", function( event ) {
             event.preventDefault();

              var btn = $(this); 
                 $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'views/ticket/' ;?>controle.php",
                data: {act:"getName",id: btn.data('id')},
                success: function (datas) {
                    var data = datas.split(';;;');
                            $('#exampleModalRight .modal-title').html("Status der Warenbewegung "+data[1]);
                            $('#idstatic').val(data[0]);
                            }
                        });

          
           
            });



              $("#expotexcel" ).on( "click", function( event ) {
            
                $( ".loadingex" ).show(); 
             var form = $( "#addform" );
             var datafrom = new FormData(document.getElementById("addform"))
             $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'views/admin/ticket/' ;?>excel.php",
                data: datafrom,
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                

                    var blob=new Blob([data]);
                var link=document.createElement('a');
                link.href=window.URL.createObjectURL(blob);
                link.download="ticket_<?php echo date('Y-m-d') ?>.xls";
                link.click();
                $( ".loadingex" ).hide();      
                         
                                              
                }
            });
           
            });
            $("#expotexceltout" ).on( "click", function( event ) {
            

                $( ".loadingex" ).show(); 
            var form = $( "#addform" );
            var datafrom = new FormData(document.getElementById("addform"))
            $.ajax({
               type: "POST",
               url: "<?php echo BASE_URL.'views/admin/ticket/' ;?>exceltout.php",
               data: datafrom,
               dataType: 'text',  // what to expect back from the PHP script, if anything
               cache: false,
               contentType: false,
               processData: false,
               success: function (data) {
               

                   var blob=new Blob([data]);
               var link=document.createElement('a');
               link.href=window.URL.createObjectURL(blob);
               link.download="ticket_<?php echo date('Y-m-d') ?>.xls";
               link.click();
               $( ".loadingex" ).hide();      
                        
                                             
               }
           });
          
           });

               });

            </script>
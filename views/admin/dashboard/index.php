<?php
if (isset($_POST['ajax'])) {
include('../../../evr.php');
}

$ticket = new ticket();

$query = $ticket->selectQuery("SELECT 
 tick.*,user.name AS user_name, abteilung.name as abteilung_name
 , unterlagen.unterlage_name ,unterlagen.original_name as original_name
 FROM ticket tick 
 JOIN user ON user.id = tick.id_user
 JOIN abteilung ON abteilung.id = tick.id_abteilung
 
 LEFT JOIN unterlagen ON unterlagen.id = tick.unterlagen
 WHERE admin_viewed='0'

 order by tick.created ASC LIMIT 10");

 $queryNum = $ticket->selectQuery("SELECT COUNT(*) as numrows FROM ticket  WHERE admin_viewed='0' ");


 $pagConfig = array(
        'totalRows' =>$queryNum[0]->numrows,
        'perPage' => $limit,
        'link_func' => 'searchFilter'
     );
         
 $pagination = new Pagination($pagConfig);

 $abteilung = new abteilung();
 $abteilungen = $abteilung->selectAll();
// $result =  connexion::getConnexion()->query( " select  count(id) as total from warenbewegungen ");
// $totalwarenbewegungen  = $result->fetch(PDO::FETCH_OBJ);


$queryNum_all = connexion::getConnexion()->query("SELECT COUNT(*) as numrows 
FROM ticket tick 
JOIN user ON user.id = tick.id_user
JOIN abteilung ON abteilung.id = tick.id_abteilung
 ");
$queryNum_all  = $queryNum_all->fetch(PDO::FETCH_OBJ);


 $queryNum_open = connexion::getConnexion()->query("SELECT COUNT(*) as numrows 
 FROM ticket tick 
 JOIN user ON user.id = tick.id_user
 JOIN abteilung ON abteilung.id = tick.id_abteilung
 WHERE status='open' ");
$queryNum_open  = $queryNum_open->fetch(PDO::FETCH_OBJ);

$queryNum_closed = connexion::getConnexion()->query("SELECT COUNT(*) as numrows 
FROM ticket tick 
JOIN user ON user.id = tick.id_user
JOIN abteilung ON abteilung.id = tick.id_abteilung
WHERE status='closed' ");
$queryNum_closed  = $queryNum_closed->fetch(PDO::FETCH_OBJ);


$queryNum_resolved = connexion::getConnexion()->query("SELECT COUNT(*) as numrows 
FROM ticket tick 
JOIN user ON user.id = tick.id_user
JOIN abteilung ON abteilung.id = tick.id_abteilung
WHERE status='resolved' ");
$queryNum_resolved  = $queryNum_resolved->fetch(PDO::FETCH_OBJ);


?>
<div class="container-fluid disable-text-selection">
    <div class="row">
        <div class="col-12">
            <div class="mb-2">
                <h1>Dashbord</h1>
                
            </div>
            
            <div class="separator mb-5"></div>
        </div>
    </div>
    <div class="row ">
        
        <div class="col-xl-3 col-lg-6 mb-4">
            
            <a href="#" class="card icon-cards-row">
                <div class="card-body text-center">
                    <i class="glyph-icon simple-icon-docs "></i>
                    <p class="card-text mb-0">GESAMTE TICKETS</p>
                    <p class="lead text-center"><?php echo $queryNum_all->numrows ?></p>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-lg-6 mb-4">
            
            <a href="#" class="card icon-cards-row">
                <div class="card-body text-center">
                    <i class="glyph-icon simple-icon-envelope-letter text-info "></i>
                    <p class="card-text mb-0">OFFENE TICKETS</p>
                    <p class="lead text-center"><?php echo $queryNum_open->numrows ?></p>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-lg-6 mb-4">
            <a href="#" class="card icon-cards-row">
                <div class="card-body text-center">
                    <i class="glyph-icon simple-icon-lock text-danger"></i>
                    <p class="card-text mb-0">GESCHLOSSENE TICKETS</p>
                    <p class="lead text-center"><?php echo $queryNum_closed->numrows ?> </p>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-lg-6 mb-4">
            
            <a href="#" class="card icon-cards-row">
                <div class="card-body text-center">
                    <i class="glyph-icon simple-icon-check text-success"></i>
                    <p class="card-text mb-0">BEHOBENE TICKETS</p>
                    <p class="lead text-center"><?php echo  $queryNum_resolved->numrows ?> </p>
                </div>
            </a>
        </div>


        <div class="col-xl-12 col-lg-12 mb-4">
        <h1>Ticket zur Überprüfung</h1>
   <div class="card h-100">
      <div class="card-body">
         <form id="addform" target="_blank" method="post" name="form_ticket" action="<?php echo BASE_URL.'views/ticket/excel.php'?>" enctype="multipart/form-data">
            <div class="form-row">
               <div class="form-group   col-md-2">
                  <label> &nbsp;&nbsp;Stichwort : </label>
                  <input type="text" id="keywords"class="form-control" placeholder="Suche mit Schlüsselwort" onkeyup="searchFilter()"/>
               </div>
               <div class="form-group col-md-2">
                  <label for="limit">limit :</label>
                  <select class="form-control select2-single"   name="limit" id="limit"  >
                     <option value="10">10</option>
                     <option value="25">25</option>
                     <option value="50">50</option>
                     <option value="100">100</option>
                     <option value="200">200</option>
                     <option value="500">500</option>
                     <option value="1000">1000</option>
                     <option value="10000">10000</option>
                  </select>
               </div>

               <div class="form-group col-md-2">
                  <label for="code_tag">Status :</label>
                  <select class="form-control select2-single"     name="status" id="status"  >
                     <option value="0">Gemeinsame Ticket</option>
                     <option value="open">öffnen</option>
                     <option value="closed">schließen</option>
                     <option value="resolved">behoben</option>
                  </select>
               </div>
               <div class="form-group col-md-2">
                  <label for="id_client">Abteilung :</label>
                  <select class="form-control select2-single" name="id_abteilung" id="id_abteilung"  >
                     <option value="0">alle</option>
                     <?php 
                        foreach ($abteilungen as $row) { 
                           echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                        }?>
                  </select>
               </div>

               <div class="form-group col-md-2">
                  <label for="dringlichkeit">Dringlichkeit :</label>
                  <select class="form-control select2-single"  name="dringlichkeit" id="dringlichkeit"  >
                           <option value="0">alle</option>
                           <option value="Niedrig">Niedrig</option>
                           <option value="Mittel">Mittel</option>
                           <option value="Höhe">Höhe</option>
                  </select>
               </div>
            
            </div>
            <div class="row">
               <div class="form-group col-md-3">
                  <label for="date_DES_1">Zeitraums Von :</label>
                  <input type="text" class="form-control datepicker filter" id="zeitpunkt_von" name="zeitpunkt_von"  >
               </div>
               <div class="form-group col-md-3">
                  <label for="date_DES_2">Zeitraums bis :</label>
                  <input type="text" class="form-control datepicker filter" id="zeitpunkt_bis" name="zeitpunkt_bis"  >
               </div>
               <div class="form-group col-md-2 text-zero">
                  <button onclick="searchFilter()" type="button" class="btn btn-success default btn-lg btn-block  mr-1 " style="margin-top: 25px;">Submit</button>
               </div>
               <div class="form-group col-md-2 text-zero">
                  <button id="expotexcel" type="button" class="btn btn-success default btn-lg btn-block  mr-1 " style="margin-top: 25px;">Export Excel</button>
               </div>
               <div class="form-group col-md-2 text-zero">
                  <button id="expotexceltout" type="button" class="btn btn-success default btn-lg btn-block  mr-1 " style="margin-top: 25px;">Export Tout</button>
               </div>
              
            </div>
            <div class="row">
             
               <div class="loadingex"></div>
            </div>
         </form>
         <div id="posts_content">
             <?php
               if (isset($_POST['ajax'])) {
                  include("../partials/table_ticket.php"); 
               }
               else
               {
                  include("admin/../partials/table_ticket.php"); 
               }
               
               ?>
         </div>
      </div>
   </div>
</div>
      <!--   <div class="col-xl-3 col-lg-6 mb-4">
            
            <a href="#" class="card icon-cards-row">
                <div class="card-body text-center">
                    <i class="iconsmind-Money-2"></i>
                    <p class="card-text mb-0">Chiffre d'affaires régler</p>
                    <p class="lead text-center"><?php echo  number_format( $totalreg->total , 2, '.', ' '); ?> DH</p>
                </div>
            </a>
        </div> -->
    </div>


    </div>

    <script type="text/javascript">
            
            function searchFilter(page_num) {
        
               
                    page_num = page_num ? page_num : 0;
                    var keywords = $('#keywords').val();
                    var status = $('#status').val();
                    var dringlichkeit = $('#dringlichkeit').val();
                    
                    var id_abteilung = $('#id_abteilung').val();
                    var zeitpunkt_von = $('#zeitpunkt_von').val();
                    var zeitpunkt_bis = $('#zeitpunkt_bis').val();
                    var limit = $('#limit').val();
                    
                    
                     $('#posts_content').html('<div class="loading"></div>');
        
        
                    $.ajax({
                        type: 'POST',
                          url: "<?php echo BASE_URL.'views/admin/dashboard/' ;?>controle.php",
                        data: {
                        "act":"pagination_viewed",
                        "keywords":keywords,
                        "page":page_num,
                        "limit":limit,
                        "status" : status,
                        "dringlichkeit" : dringlichkeit,
                        "id_abteilung" : id_abteilung,
                        "zeitpunkt_von" : zeitpunkt_von,
                        "zeitpunkt_bis" : zeitpunkt_bis,
                         },
                        beforeSend: function () {
                            $('.loading-overlay').show();
                        },
                        success: function (html) {
                            $('#posts_content').html(html);
        
                 table =  $('#myTable').DataTable( {
                            responsive: true,
                           
                          
                            bPaginate: false, 
                            bFilter: false, 
                            bInfo: false,
                           
                              
                              
                 });
                 
        
                        }
                    });
                }
        
        
        
                    $(document).ready(function () {
        
        
         
        
         $(".select2-single").select2({
                    theme: "bootstrap",
                    placeholder: "",
                    maximumSelectionSize: 6,
                    containerCssClass: ":all:"
                });
                        $("input.datepicker").datepicker({
                             format: 'yyyy-mm-dd',
                             templates: {
                             leftArrow: '<i class="simple-icon-arrow-left"></i>',
                             rightArrow: '<i class="simple-icon-arrow-right"></i>'
                            }
                        })
         $('#myTable').DataTable( {
                            responsive: true,
                           
                          
                            bPaginate: false, 
                            bFilter: false, 
                            bInfo: false,
                        } );
                    
                  
        
        
        
        
        
                      $("#expotexcel" ).on( "click", function( event ) {
                    
                        $( ".loadingex" ).show(); 
                     var form = $( "#addform" );
                     var datafrom = new FormData(document.getElementById("addform"))
                     $.ajax({
                        type: "POST",
                        url: "<?php echo BASE_URL.'views/admin/ticket/' ;?>excel.php",
                        data: datafrom,
                        dataType: 'text',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                        
        
                        var blob=new Blob([data]);
                        var link=document.createElement('a');
                        link.href=window.URL.createObjectURL(blob);
                        link.download="ticket_<?php echo date('Y-m-d') ?>.xls";
                        link.click();
                        $( ".loadingex" ).hide();      
                                 
                                                      
                        }
                    });
                   
                    });
                    $("#expotexceltout" ).on( "click", function( event ) {
                    
        
                        $( ".loadingex" ).show(); 
                    var form = $( "#addform" );
                    var datafrom = new FormData(document.getElementById("addform"))
                    $.ajax({
                       type: "POST",
                       url: "<?php echo BASE_URL.'views/ticket/' ;?>exceltout.php",
                       data: datafrom,
                       dataType: 'text',  // what to expect back from the PHP script, if anything
                       cache: false,
                       contentType: false,
                       processData: false,
                       success: function (data) {
                       
        
                           var blob=new Blob([data]);
                       var link=document.createElement('a');
                       link.href=window.URL.createObjectURL(blob);
                       link.download="ticket_<?php echo date('Y-m-d') ?>.xls";
                       link.click();
                       $( ".loadingex" ).hide();      
                                
                                                     
                       }
                   });
                  
                   });
        
                       });
        
                    </script>


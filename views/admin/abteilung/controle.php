<?php
include('../../../evr.php');

if ($_POST['act']=='insert') {
	try {
		$_POST["id_user"] = auth::user()["id"] ;
		$abteilung=new abteilung(); 
		$res = $abteilung->insert();
		if ($res) {
			die('success');
		}
		die("Erreur");
		} catch (Exception $e) {
				die($e);
	}
}
elseif ($_POST['act']=='update') {
	try {
		$_POST["id_user"] = auth::user()["id"] ;       
		$abteilung=new abteilung();
 		$abteilung->update($_POST["id"]);
		die('success');
	} 
	catch (Exception $e) {
				die($e);
		
	}
}
elseif ($_POST['act']=='delete') {
	try {
		$abteilung=new abteilung();
		$abteilung->delete($_POST["id"]);
		die('success');
		} catch (Exception $e) {
				die($e);
	}
}

?>
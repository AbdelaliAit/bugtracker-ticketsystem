<?php
if(count((array)$query)>0){ ?>
	  

	<table class="table datatables table-striped table-bordered" id="myTable" style="width: 100%">
					<thead>
                     <tr>
                        <th>ID</th>
                        <th>Code</th>
                        <th>Status</th>
                        <th>Titel</th>
                        <th>User</th>
                        <th>Abteilung</th>
                        <th>Dringlichkeit</th>
                        <th>Veroeffentlicht am</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php 
                        $i=0;
                     foreach($query as $ligne) { 
                     $i++; 
                                            
                           if($ligne->status=="open")
                           {
                              $status="OFFEN";

                           }if($ligne->status=="closed")
                           {
                              $status="GESCHLOSSEN";
                           }
                           if($ligne->status=="resolved")
                           {
                             $status="BEHOBEN";
                           }


                          if($ligne->dringlichkeit=="Niedrig")
                          {
                              $dringlichkeit="Niedrig";
                          
                          }if($ligne->dringlichkeit=="Mittel")
                          {
                              $dringlichkeit="Mittel";
                          }
                          if($ligne->dringlichkeit=="Höhe")
                          {
                              $dringlichkeit="Hoehe";
                          }
                     ?>
                     <tr>
                        <td> <?php echo $ligne->id ; ?></td>
                        <td> <?php echo $ligne->code ; ?></td>
                        <td> 
                        <?php echo $status; ?>
                        </td>
                        <td> <?php echo $ligne->titel; ?> </td>
                        <td> <?php echo $ligne->user_name; ?> </td>
                        <td> <?php echo $ligne->abteilung_name; ?> </td>
                        <td> 
                        <?php echo htmlentities(utf8_decode($dringlichkeit)); ?>
                        </td>
                        <td> <?php echo date('Y-m-d',strtotime($ligne->created));  ?></td>
                       
                     </tr>
                     <?php } ?>
                  </tbody>
               </table>

   <?php }


else echo " <center> <strong style='color: red; '> Nichts wurde gefunden</strong></center>";


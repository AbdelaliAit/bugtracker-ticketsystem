<?php
if(count((array)$query)>0){ ?>
	<div class="table-responsive ">
	<h1>Gesamte ticket : <?php echo $queryNum[0]->numrows ; ?></h1>
               

	<table class="table datatables table-striped table-bordered" id="myTable" style="width: 100%">
					<thead>
                     <tr>
                        <th>ID</th>
                        <th>Code</th>
                        <th>Status</th>
                        <th>Titel</th>
                        <th>User</th>
                        <th>Abteilung</th>
                        <th>Dringlichkeit</th>
                        <th>Veröffentlicht am</th>
                        <th>Aktionen</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php 
                        $i=0;
                     foreach($query as $ligne) { 
                     $i++; 
                                            
                           if($ligne->status=="open")
                           {
                              $status="<span class='badge badge-info'>OFFEN</span>";

                           }if($ligne->status=="closed")
                           {
                              $status="<span class='badge badge-danger'>GESCHLOSSEN</span>";
                           }
                           if($ligne->status=="resolved")
                           {
                             $status="<span class='badge badge-success'>BEHOBEN</span>";
                           }


                          if($ligne->dringlichkeit=="Niedrig")
                          {
                              $dringlichkeit="<span class='badge badge-info'>Niedrig</span>";
                          
                          }if($ligne->dringlichkeit=="Mittel")
                          {
                              $dringlichkeit="<span class='badge badge-warning'>Mittel</span>";
                          }
                          if($ligne->dringlichkeit=="Höhe")
                          {
                              $dringlichkeit="<span class='badge badge-danger'>Höhe</span>";
                          }
                     ?>
                     <tr>
                        <td> <?php echo $ligne->id ; ?></td>
                        <td> <?php echo $ligne->code ; ?></td>
                        <td> 
                        <?php echo $status; ?>
                        </td>
                        <td> <?php echo $ligne->titel; ?> </td>
                        <td> <?php echo $ligne->user_name; ?> </td>
                        <td> <?php echo $ligne->abteilung_name; ?> </td>
                        <td> 
                        <?php echo $dringlichkeit; ?>
                        </td>
                        <td> <?php echo date('Y-m-d',strtotime($ligne->created));  ?></td>
                       

                        
                        <td>
                           <a class="badge badge-primary mb-2  url notlink" data-url="admin/ticket/ticket-details.php?id=<?php echo $ligne->id;?>" style="color: white;cursor: pointer;" title="bearbeiten"
                              href="javascript:void(0)">
                              <i class="glyph-icon simple-icon-eye" style="font-size: 15px;"> </i>
                           </a>
                           
                        </td>
                     </tr>
                     <?php } ?>
                  </tbody>
               </table>

    </div>
                            <?php echo $pagination->createLinks(); ?>
   <?php }


else echo " <center> <strong style='color: red; '> Nichts wurde gefunden</strong></center>";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE IF NOT EXISTS `abteilung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

DELETE FROM `abteilung`;
/*!40000 ALTER TABLE `abteilung` DISABLE KEYS */;
INSERT INTO `abteilung` (`id`, `name`) VALUES
	(1, 'IT'),
	(3, 'WIRTSCHAFT');
/*!40000 ALTER TABLE `abteilung` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(250) DEFAULT NULL,
  `titel` varchar(255) DEFAULT NULL,
  `inhalt` varchar(1000) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_abteilung` int(11) DEFAULT NULL,
  `unterlagen` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('open','closed','resolved') NOT NULL DEFAULT 'open',
  `dringlichkeit` enum('Niedrig','Mittel','Höhe') NOT NULL DEFAULT 'Niedrig',
  `user_viewed` int(11) NOT NULL DEFAULT '1',
  `admin_viewed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

DELETE FROM `ticket`;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` (`id`, `code`, `titel`, `inhalt`, `id_user`, `id_abteilung`, `unterlagen`, `created`, `status`, `dringlichkeit`, `user_viewed`, `admin_viewed`) VALUES
	(25, 'skvlEw', 'How can I echo HTML in PHP?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 42, 1, 6, '2021-04-02 23:38:09', 'open', 'Mittel', 1, 1),
	(26, 'kGxLjI', 'test', '&lt;p&gt;svsdv&lt;/p&gt;', 42, 1, 14, '2021-04-03 00:41:49', 'open', 'Niedrig', 1, 0);
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `ticket_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ticket` int(11) NOT NULL,
  `unterlagen` varchar(250) NOT NULL,
  `nachricht` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

DELETE FROM `ticket_comment`;
/*!40000 ALTER TABLE `ticket_comment` DISABLE KEYS */;
INSERT INTO `ticket_comment` (`id`, `id_ticket`, `unterlagen`, `nachricht`, `id_user`, `created`) VALUES
	(8, 25, '12', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n', 41, '2021-04-03 00:05:42'),
	(9, 25, '12', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n', 42, '2021-04-03 00:05:42'),
	(10, 25, '13', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions ', 42, '2021-04-03 00:23:47'),
	(11, 25, '12', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n', 41, '2021-04-03 00:05:42'),
	(13, 26, '15', 'test', 41, '2021-04-03 00:49:55'),
	(14, 26, '16', 'sgers gwgww', 42, '2021-04-03 00:53:15');
/*!40000 ALTER TABLE `ticket_comment` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `unterlagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `unterlage_name` varchar(250) NOT NULL,
  `original_name` varchar(250) NOT NULL,
  `unterlage_size` varchar(250) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

DELETE FROM `unterlagen`;
/*!40000 ALTER TABLE `unterlagen` DISABLE KEYS */;
INSERT INTO `unterlagen` (`id`, `id_user`, `unterlage_name`, `original_name`, `unterlage_size`, `created`) VALUES
	(1, 42, 'd60557ad5cd3c9fc6b7f5b7ff5144a8e.sql', '', '14032', '2021-04-02 17:09:03'),
	(2, 42, '7414de75a1e197e72f3435d6ca13161a.sql', '', '14032', '2021-04-02 17:09:23'),
	(3, 42, '7ba3c246e4784aa7a26827e3dd25f887.sql', '314799.jpg', '14032', '2021-04-02 17:09:34'),
	(4, 42, 'bd71055f177cc12012c17aa0a166c195.jpg', '314799.jpg', '249391', '2021-04-02 22:44:51'),
	(5, 42, '0f3d7405c6791ef3da7a8bd8b02f10b9.jpg', '314799.jpg', '249391', '2021-04-02 23:01:20'),
	(6, 42, 'a7d44e22401cfa23b89e1191abacf303.jpg', 'ilyasse-new-cv.jpg', '1688185', '2021-04-02 23:38:09'),
	(7, 41, 'cc9be684a8831eb77dff6099c60e6b06.png', 'casa-wood.png', '2761', '2021-04-02 23:51:25'),
	(8, 41, '0d5bbb440df4332669f2c1a9fd6c4f27.png', 'casa-wood.png', '2761', '2021-04-02 23:52:56'),
	(9, 41, 'c08de7f7b9956bd9f8410a8b57909612.png', 'casa-wood.png', '2761', '2021-04-02 23:59:11'),
	(10, 41, 'f0cf99da31a38a1956f202106bf543c2.png', 'casa-wood.png', '2761', '2021-04-03 00:01:18'),
	(11, 41, '9a2cae5b9617286d896b1f98b83c81f9.png', 'casa-wood.png', '2761', '2021-04-03 00:03:20'),
	(12, 41, '2df47ae117c1175899657794880e886a.png', 'casa-wood.png', '16881851', '2021-04-03 00:05:42'),
	(13, 42, '8b5965aae96ed91fe38508259d2b15cb.jpg', '314799.jpg', '249391', '2021-04-03 00:23:47'),
	(14, 42, '0f7afe09b920753b600a6035649ceada.png', 'casa-wood.png', '2761', '2021-04-03 00:41:49'),
	(15, 41, 'e0f848125cf0ae7ecd2c34a8ccda2b65.png', 'casa-wood.png', '2761', '2021-04-03 00:49:55'),
	(16, 42, '3637f5e48c52e003e41273362cfb5522.jpg', '314799.jpg', '249391', '2021-04-03 00:53:14');
/*!40000 ALTER TABLE `unterlagen` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `pwd` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `type` varchar(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `login`, `name`, `pwd`, `email`, `type`) VALUES
	(41, 'admin', 'admin', 'admin', 'admin@gmail.com', '1'),
	(42, 'user', 'user', 'user', 'user@gmail.com', '0');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
